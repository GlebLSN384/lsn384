const { v4: uuidV4 } = require('uuid')
const { Sequelize, Model, DataTypes } = require('sequelize')

let accessModelInDB;

const driver = {}

driver.create = async (modelName, data) => {
    const accessor = accessModelInDB(modelName)

    const item = await accessor.create({
        id: uuidV4(),
        text: data.text
    })
    
    return item
} // item | throw Error
driver.read = async (modelName, id) => {
    const accessor = accessModelInDB(modelName)

    const item = accessor.findOne({ where: { id } })

    if(!item) throw new Error(`No item with id '${id}' in '${modelName}' model`)

    return item
} // item | throw Error
driver.update = async (modelName, id, data) => {
    const accessor = accessModelInDB(modelName)

    let count = await accessor.update({
        ...data,
        id
    }, { where: { id } })

    if(count[0] !== 1) throw new Error(`Something go wrong when try to update item with ${id} from '${modelName}' model store`)

    return await accessor.findOne({ where: { id } })
} // item | throw Error
driver.delete = async (modelName, id) => {
    const accessor = accessModelInDB(modelName)

    let count = await accessor.destroy({ where: { id } })

    if(count !== 1) throw new Error(`Something go wrong when try to delete item with ${id} from '${modelName}' model store`)

    return { result: true }
} // { result: true } | throw Error
driver.list = async (modelName) => {
    const accessor = accessModelInDB(modelName)

    const items = accessor.findAll() // TODO: pagination

    return items
} // item[] | throw Error
driver.init = async () => {
    const sequelize = new Sequelize('postgres://belg:password@192.168.0.2:5432/loft-pg')

    const messages = sequelize.define("messages", {
        id: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        text: DataTypes.TEXT,
    });


    const models = {
        messages
    }

    accessModelInDB = function (modelName) {
        if(models[modelName]) return models[modelName]
    
        throw new Error(`Model with name '${modelName}' not exist in memory`)
    }

    await sequelize.sync({ force: true });
}

module.exports = driver

