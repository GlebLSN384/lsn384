const { v4: uuidV4 } = require('uuid')

const driver = {}

const memory = {}

driver.create = async (modelName, data) => {
    const accessor = accessOrCreateModelInMemory(modelName)

    const newLenght = accessor.push({ ...data, id: uuidV4(), date: new Date() })

    return accessor[newLenght - 1]
} // item | throw Error
driver.read = async (modelName, id) => {
    const accessor = accessModelInMemory(modelName)

    const item = accessor.find(i => i.id === id)

    if(!item) throw new Error(`No item with id '${id}' in '${modelName}' model`)

    return item
} // item | throw Error
driver.update = async (modelName, id, data) => {
    let accessor = accessModelInMemory(modelName)

    let item = accessor.find(i => i.id === id)

    if(!item) throw new Error(`No item with id '${id}' in '${modelName}' model, nothing to update`)

    item = {
        ...item,
        ...data,
        id: item.id,
        date: new Date()
    }

    memory[modelName] = accessor.map(i => i.id === id ? item : i)

    return item
} // item | throw Error
driver.delete = async (modelName, id) => {
    let accessor = accessModelInMemory(modelName)
    const initialLength = accessor.length

    accessor = accessor.filter(item => item.id !== id)

    const length = accessor.length

    if(initialLength - 1 === length) {
        memory[modelName] = accessor
        return { result: true }
    }

    throw new Error(`Something go wrong when try to delete item with ${id} from '${modelName}' model store`)
} // { result: true } | throw Error
driver.list = async (modelName) => {
    const accessor = accessModelInMemory(modelName)

    return accessor
} // item[] | throw Error
driver.init = async () => {
    return;
}

function accessModelInMemory(modelName) {
    if(memory[modelName]) return memory[modelName]

    throw new Error(`Model with name '${modelName}' not exist in memory`)
}

function accessOrCreateModelInMemory(modelName) {
    if(!memory[modelName]) memory[modelName] = []
    
    return memory[modelName]
}

module.exports = driver