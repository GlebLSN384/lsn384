const handlers = {};

const postgresDriver = require('./postgres-driver')
const memoryDriver = require('./memory-driver')

const driver = process.env.DB === 'postgres' ? postgresDriver : memoryDriver;
(async () => {
    await driver.init()
})()

handlers.ping = function (data, callback) {
    callback(200, { pong: true })
};

handlers.notFound = function (data, callback) {
    callback(404);
};

handlers.messages = function (data, callback) {
    const acceptableMethods = ['post', 'get', 'put', 'delete'];

    if (acceptableMethods.includes(data.method)) {
        handlers._messages[data.method](data, callback);
    } else {
        callback(405);
    }
}

handlers._messages = {}

handlers._messages.post = async function (data, callback) {
    const text = data.payload.text

    try {
        const item = await driver.create('messages', { text })
        callback(200, item)
    } catch (error) {
        callback(500, { error: error?.message || error });
    }

}
handlers._messages.get = async function (data, callback) {
    const id = data.queryStringObject.id

    try {
        if(id) {
            console.log(id);
            const item = await driver.read('messages', id)
            callback(200, item)
        } else {
            const items = await driver.list('messages')
            callback(200, items)
        }
    } catch (error) {
        callback(500, { error: error?.message || error });
    }

}
handlers._messages.put = async function (data, callback) {
    const id = data.payload.id
    const text = data.payload.text

    try {
        const item = await driver.update('messages', id, { text })
        callback(200, item)
    } catch (error) {
        callback(500, { error: error?.message || error });
    }

}
handlers._messages.delete = async function (data, callback) {
    const id = data.payload.id

    try {
        const item = await driver.delete('messages', id)
        callback(200, item)
    } catch (error) {
        callback(500, { error: error?.message || error });
    }

}

module.exports = handlers;