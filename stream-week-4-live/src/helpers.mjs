export const parseJsonToObject = function(str) {
  try {
    return JSON.parse(str);
  } catch (e) {
    return {};
  }
};
