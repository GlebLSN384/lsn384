import http from 'http';
import url from 'url';
import { StringDecoder } from 'string_decoder';

import { config } from './config.mjs'
import { notFoundHandler, messagesHandler } from './handlers/index.mjs'
import { parseJsonToObject } from './helpers.mjs'

const router = {
  messages: messagesHandler
};

const httpServer = http.createServer(((req, res) => {
  const parseUrl = url.parse(req.url, true);

  const path = parseUrl.pathname;
  const trimmedPath = path.replace(/^\/+|\/+$/g, '');

  const method = req.method.toLowerCase();

  const queryStringObject = parseUrl.query;

  const headers = req.headers;

  const decoder = new StringDecoder('utf-8');
  let buffer = '';
  req.on('data', function(data) {
    buffer += decoder.write(data);
  });
  req.on('end', function() {
    buffer += decoder.end();

    const chooseHandler = typeof (router[trimmedPath]) !== 'undefined'
      ? router[trimmedPath]
      : notFoundHandler;

    const data = {
      trimmedPath,
      queryStringObject,
      method,
      headers,
      payload: parseJsonToObject(buffer)
    };

    chooseHandler(data, function(statusCode, payload) {
      statusCode = typeof statusCode === 'number' ? statusCode : 200;

      payload = typeof payload === 'object' ? payload : {};

      const payloadString = JSON.stringify(payload);

      res.setHeader('content-type', 'application/json');
      res.writeHead(statusCode);
      res.end(payloadString);
    });

  });
}))

export const init = () => {
  httpServer.listen(config.httpPort, function() {
    console.log(`The http  server listen on ${config.httpPort} port in ${config.envName} mode`);
  });
};
