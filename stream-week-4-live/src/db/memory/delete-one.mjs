import { accessOrCreateModelInMemory } from './util.mjs';
import { memory } from './memory.mjs';

export const deleteOne = async (modelName, id) => {
  let accessor = accessOrCreateModelInMemory(modelName);
  const initialLength = accessor.length;

  accessor = accessor.filter(item => item.id !== id);

  const length = accessor.length;

  if (initialLength - 1 === length) {
    memory[modelName] = accessor;
    return { result: true };
  }

  throw new Error(`Something go wrong when try to delete item with ${id} from '${modelName}' model store`);
};
