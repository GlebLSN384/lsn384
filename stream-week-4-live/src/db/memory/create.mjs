import { v4 as uuidV4 } from 'uuid';

import { accessOrCreateModelInMemory } from './util.mjs';

export const create = async (modelName, data) => {
  const accessor = accessOrCreateModelInMemory(modelName);

  const newLength = accessor.push({ ...data, id: uuidV4(), date: new Date() });

  return accessor[newLength - 1];
};
