import { deleteOne } from './delete-one.mjs'
import { create } from './create.mjs'
import { list } from './list.mjs'
import { read } from './read.mjs'
import { update } from './update.mjs'

export const driver = {
  deleteOne,
  create,
  list,
  read,
  update
}
