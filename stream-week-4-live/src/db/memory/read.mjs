import { accessOrCreateModelInMemory } from './util.mjs';

export const read = async (modelName, id) => {
  const accessor = accessOrCreateModelInMemory(modelName);

  const item = accessor.find(i => i.id === id);

  if (!item) throw new Error(`No item with id '${id}' in '${modelName}' model`);

  return item;
};
