import { memory } from './memory.mjs';

export function accessOrCreateModelInMemory(modelName) {
  if (!memory[modelName]) memory[modelName] = [];

  return memory[modelName];
}
