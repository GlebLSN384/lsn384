import { accessOrCreateModelInMemory } from './util.mjs';

export const list = async (modelName) => {
  return accessOrCreateModelInMemory(modelName);
};
