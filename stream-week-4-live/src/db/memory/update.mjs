import { accessOrCreateModelInMemory } from './util.mjs';
import { memory } from './memory.mjs';

export const update = async (modelName, id, data) => {
  let accessor = accessOrCreateModelInMemory(modelName);

  let item = accessor.find(i => i.id === id);

  if (!item) throw new Error(`No item with id '${id}' in '${modelName}' model, nothing to update`);

  item = {
    ...item,
    ...data,
    id: item.id,
    date: new Date()
  };

  memory[modelName] = accessor.map(i => i.id === id ? item : i);

  return item;
};
