import { handler as messageHandler } from './messages/index.mjs';

export const notFoundHandler = function(data, callback) {
  callback(404);
};

export const messagesHandler = function(data, callback) {
  const acceptableMethods = ['post', 'get', 'put', 'delete'];

  if (acceptableMethods.includes(data.method)) {
    messageHandler[data.method](data, callback);
  } else {
    callback(405);
  }
};
