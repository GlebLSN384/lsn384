import { postHandler } from './post-handler.mjs';
import { getHandler } from './get-handler.mjs';
import { putHandler } from './put-handler.mjs';
import { deleteHandler } from './delete-handler.mjs';

export const handler = {
  post: postHandler,
  get: getHandler,
  put: putHandler,
  delete: deleteHandler
}
