import { db } from '../../db/index.mjs';

export const postHandler = async function(data, callback) {
  const text = data.payload.text;

  try {
    const item = await db.create('messages', { text });
    callback(200, item);
  } catch (error) {
    callback(500, { error: error?.message || error });
  }
};
