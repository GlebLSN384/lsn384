import { db } from '../../db/index.mjs';

export const deleteHandler = async function(data, callback) {
  const id = data.payload.id;

  try {
    const item = await db.deleteOne('messages', id);
    callback(200, item);
  } catch (error) {
    callback(500, { error: error?.message || error });
  }
};
