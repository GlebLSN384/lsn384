import { db } from '../../db/index.mjs';

export const getHandler = async function(data, callback) {
  const id = data.queryStringObject.id;

  try {
    if (id) {
      const item = await db.read('messages', id);
      callback(200, item);
    } else {
      const items = await db.list('messages');
      callback(200, items);
    }
  } catch (error) {
    callback(500, { error: error?.message || error });
  }
};
