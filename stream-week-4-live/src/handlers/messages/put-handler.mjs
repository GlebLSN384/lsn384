import { db } from '../../db/index.mjs';

export const putHandler = async function(data, callback) {
  const id = data.payload.id;
  const text = data.payload.text;

  try {
    const item = await db.update('messages', id, { text });
    callback(200, item);
  } catch (error) {
    callback(500, { error: error?.message || error });
  }

};
