import { init as serverInit } from './server.mjs';

import logger from 'loglevel'

const isProduction = process.env.NODE_ENV === 'production'
const logLevel = isProduction ? 'warn' : 'trace'

logger.setLevel(logLevel)

const init = () => {
  serverInit();
};

init();
