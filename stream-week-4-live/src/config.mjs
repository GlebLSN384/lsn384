const config = process.env.NODE_ENV !== 'production'
  ? {
    httpPort: process.env.PORT || 3000,
    envName: 'development',
    hashingSecret: 'hashingSecret'
  }
  : {
    httpPort: 8080,
    envName: 'production',
    hashingSecret: 'hashingSecret'
  };

export {
  config
};
