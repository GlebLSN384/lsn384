const path = require('path');

const server = require(path.resolve(__dirname, './lib/server'));

const app = {};

app.init = () => {
    server.init();
}
app.init();

module.exports = app;
