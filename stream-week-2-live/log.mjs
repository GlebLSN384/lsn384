// const log = require('loglevel')
import log from 'loglevel'
import { v4 } from 'uuid'

if (process.env.NODE_ENV !== 'development') {
    log.setLevel('error')
} else {
    log.setLevel('trace')
    log.info('This is not production start!')
}

log.error('This is error')
log.info(v4())


