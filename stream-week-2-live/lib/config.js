const environments = {};

environments.staging = {
    httpPort: 3000,
    httpsPort: 3001,
    envName: 'staging',
    hashingSecret: 'hashingSecret'
}

environments.production = {
    httpPort: 5000,
    httpsPort: 5001,
    envName: 'production',
    hashingSecret: 'hashingSecret'
}

const currentEnvironment = process.env.NODE_ENV?.toLowerCase();

module.exports = environments[currentEnvironment] || environments.staging;
