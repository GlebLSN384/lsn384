const handlers = {};

handlers.ping = function (data, callback) {
    callback(200, { pong: true })
};

handlers.notFound = function (data, callback) {
    callback(404);
};

module.exports = handlers;