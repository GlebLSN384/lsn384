const crypto = require('crypto');
const path = require('path');

const config = require(path.resolve(__dirname, './config'));

const helpers = {};

helpers.hash = function (str) {
    if(typeof str === 'string' && str.length > 0) {
        return crypto.createHmac('sha256', config.hashingSecret).update(str).digest('hex');
    } else {
        return false;
    }
}

helpers.parseJsonToObject = function (str) {
    try {
        return JSON.parse(str);
    } catch (e) {
        return {}
    }
}

helpers.createRandomString = function (strLength) {
    strLength = typeof(strLength) === 'number' && strLength > 0 ? strLength : false;

    if (strLength) {
        const possiblaCharsacters = '0123456789abcdef';
        let str = '';
        while (str.length !== strLength) {
            const randomCharacter = possiblaCharsacters.charAt(Math.floor(Math.random() * possiblaCharsacters.length));
            str = str + randomCharacter
        }
        return str;
    } else {
        return false;
    }
}


module.exports = helpers;
