const http = require('http');
const https = require('https');
const url = require('url');
const StringDecoder = require('string_decoder').StringDecoder;
const fs = require('fs');
const path = require('path');

const config = require(path.resolve(__dirname, './config'));
const handlers = require(path.resolve(__dirname, './handlers'));
const helpers = require(path.resolve(__dirname, './helpers'));

const server = {}

server.httpServer = http.createServer(( (req, res) => server.unifiedServer(req, res)));

server.httpsServerOptions = {
    key: fs.readFileSync(path.resolve(__dirname, '../https', 'key.pem')),
    cert: fs.readFileSync(path.resolve(__dirname, '../https', 'cert.pem')),
}

server.httpsServer = https.createServer(server.httpsServerOptions, ( (req, res) => server.unifiedServer(req, res)));

server.unifiedServer = (req, res) => {

    const parseUrl = url.parse(req.url, true);

    const path = parseUrl.pathname;
    const trimmedPath = path.replace(/^\/+|\/+$/g, '');

    const method = req.method.toLowerCase();

    const queryStringObject = parseUrl.query;

    const headers = req.headers;

    const decoder = new StringDecoder('utf-8');
    let buffer = '';
    req.on('data', function (data) {
        buffer += decoder.write(data);
    });
    req.on('end', function () {
        buffer += decoder.end();

        const chooseHandler = typeof (server.router[trimmedPath]) !== "undefined"
            ? server.router[trimmedPath]
            : handlers.notFound;

        const data = {
            trimmedPath,
            queryStringObject,
            method,
            headers,
            payload: helpers.parseJsonToObject(buffer),
        };
        // console.log(data)

        chooseHandler(data, function (statusCode, payload) {
            statusCode = typeof statusCode === "number" ? statusCode : 200;

            payload = typeof payload === 'object' ? payload : {};

            const payloadString = JSON.stringify(payload)

            res.setHeader('content-type', 'application/json');
            res.writeHead(statusCode)
            res.end(payloadString);
        });

    });
}

server.router = {
    ping: handlers.ping,
}

server.init = () => {
    server.httpServer.listen(config.httpPort, function () {
        console.log(`The http  server listen on ${config.httpPort} port in ${config.envName} mode`)
    })
    server.httpsServer.listen(config.httpsPort, function () {
        console.log(`The https server listen on ${config.httpsPort} port in ${config.envName} mode`)
    })
}

module.exports = server;
