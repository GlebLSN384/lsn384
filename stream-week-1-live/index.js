const yargs = require('yargs')
const path = require('path')
const { version } = require('./package.json')
const fs = require('fs')
const { promisify } = require('util')

// f( p1, p2, (err, ...) => )

const FANCY_APP_ENV_PREFIX = process.env['FANCY_APP_ENV_PREFIX'] || 'FANCY_APP'

const argv = yargs
    .usage('Usage: node $0 [options...]')
    .version(version)
    .alias('version', 'v')
    .help('help')
    .env(FANCY_APP_ENV_PREFIX)
    .alias('help', 'h')
    .example('yarn start')
    .option('input', {
        alias: 'i',
        describe: 'Path to input folder',
        required: true
    })
    .option('output', {
        alias: 'o',
        describe: 'Path to output folder',
        default: './output'
    })
    .option('delete', {
        alias: 'd',
        describe: 'Should be source folder deleted?',
        type: 'boolean',
        default: false
    })
    .argv

const config = {
    paths: {
        input: path.normalize(path.join(__dirname, argv.input)),
        output: path.normalize(path.join(__dirname, argv.output))
    },
    shouldDelete: argv.delete
}

let canNotDelete = []

const REQUIRED_FILE_PERMISSION = config.shouldDelete ? fs.constants.R_OK | fs.constants.W_OK : fs.constants.R_OK

fs.access(config.paths.input, REQUIRED_FILE_PERMISSION, async (err) => {
    if (err) {
        process.exit(err)
    }

    const stat = promisify(fs.stat)
    const st = await stat(config.paths.input)
    console.log({ st, isD: st.isDirectory() })

    fs.stat(config.paths.input, (err, stat) => {
        console.log({ stat, isD: stat.isDirectory() })
        fs.stat(config.paths.input, (err, stat) => {
            console.log({ stat, isD: stat.isDirectory() })
            fs.stat(config.paths.input, (err, stat) => {
                console.log({ stat, isD: stat.isDirectory() })
                fs.stat(config.paths.input, (err, stat) => {
                    console.log({ stat, isD: stat.isDirectory() })
                    fs.stat(config.paths.input, (err, stat) => {
                        console.log({ stat, isD: stat.isDirectory() })
                        fs.stat(config.paths.input, (err, stat) => {
                            console.log({ stat, isD: stat.isDirectory() })
                            fs.stat(config.paths.input, (err, stat) => {
                                console.log({ stat, isD: stat.isDirectory() })
                        
                            })
                        })
                    })
                })
            })
        })
    })

})

process.on('exit', (error) => {
    console.error(error)
})